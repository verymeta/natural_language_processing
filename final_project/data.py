"""Dataset loader and data utilities.

Author:
    Shrey Desai and Yasumasa Onoe
"""

import collections
import itertools
import torch

from torch.utils.data import Dataset
from random import shuffle
from utils import cuda, load_dataset

import spacy

SPACY_NLP = spacy.load("en_core_web_sm",
        disable=["tagger", "parser", "lemmatizer", "attribute_ruler"]
        )
# disable the rest, it slows us down
# SPACY_NLP.select_pipes(enable="ner")

import diskcache as dc
CACHE = dc.Cache('tmp')

import pickle

PAD_TOKEN = '[PAD]'
UNK_TOKEN = '[UNK]'

NER_VOCAB = { 0: 0 }

class Vocabulary:
    """
    This class creates two dictionaries mapping:
        1) words --> indices,
        2) indices --> words.

    Args:
        samples: A list of training examples stored in `QADataset.samples`.
        vocab_size: Int. The number of top words to be used.

    Attributes:
        words: A list of top words (string) sorted by frequency. `PAD_TOKEN`
            (at position 0) and `UNK_TOKEN` (at position 1) are prepended.
            All words will be lowercased.
        encoding: A dictionary mapping words (string) to indices (int).
        decoding: A dictionary mapping indices (int) to words (string).
    """
    def __init__(self, samples, vocab_size):
        self.words = self._initialize(samples, vocab_size)
        self.encoding = {word: index for (index, word) in enumerate(self.words)}
        self.decoding = {index: word for (index, word) in enumerate(self.words)}

    def _initialize(self, samples, vocab_size):
        """
        Counts and sorts all tokens in the data, then it returns a vocab
        list. `PAD_TOKEN and `UNK_TOKEN` are added at the beginning of the
        list. All words are lowercased.

        Args:
            samples: A list of training examples stored in `QADataset.samples`.
            vocab_size: Int. The number of top words to be used.

        Returns:
            A list of top words (string) sorted by frequency. `PAD_TOKEN`
            (at position 0) and `UNK_TOKEN` (at position 1) are prepended.
        """
        vocab = collections.defaultdict(int)
        for (_, passage, question, _, _, _, _) in samples:
            for token in itertools.chain(passage, question):
                vocab[token.lower()] += 1
        top_words = [
            word for (word, _) in
            sorted(vocab.items(), key=lambda x: x[1], reverse=True)
        ][:vocab_size]
        words = [PAD_TOKEN, UNK_TOKEN] + top_words
        return words
    
    def __len__(self):
        return len(self.words)


class Tokenizer:
    """
    This class provides two methods converting:
        1) List of words --> List of indices,
        2) List of indices --> List of words.

    Args:
        vocabulary: An instantiated `Vocabulary` object.

    Attributes:
        vocabulary: A list of top words (string) sorted by frequency.
            `PAD_TOKEN` (at position 0) and `UNK_TOKEN` (at position 1) are
            prepended.
        pad_token_id: Index of `PAD_TOKEN` (int).
        unk_token_id: Index of `UNK_TOKEN` (int).
    """
    def __init__(self, vocabulary):
        self.vocabulary = vocabulary
        self.pad_token_id = self.vocabulary.encoding[PAD_TOKEN]
        self.unk_token_id = self.vocabulary.encoding[UNK_TOKEN]

    def convert_tokens_to_ids(self, tokens):
        """
        Converts words to corresponding indices.

        Args:
            tokens: A list of words (string).

        Returns:
            A list of indices (int).
        """
        return [
            self.vocabulary.encoding.get(token, self.unk_token_id)
            for token in tokens
        ]

    def convert_ids_to_tokens(self, token_ids):
        """
        Converts indices to corresponding words.

        Args:
            token_ids: A list of indices (int).

        Returns:
            A list of words (string).
        """
        return [
            self.vocabulary.decoding.get(token_id, UNK_TOKEN)
            for token_id in token_ids
        ]


class QADataset(Dataset):
    """
    This class creates a data generator.

    Args:
        args: `argparse` object.
        path: Path to a data file (.gz), e.g. "datasets/squad_dev.jsonl.gz".

    Attributes:
        args: `argparse` object.
        meta: Dataset metadata (e.g. dataset name, split).
        elems: A list of raw examples (jsonl).
        samples: A list of preprocessed examples (tuple). Passages and
            questions are shortened to max sequence length.
        tokenizer: `Tokenizer` object.
        batch_size: Int. The number of example in a mini batch.
    """
    def __init__(self, args, path):
        self.args = args
        self.meta, self.elems = load_dataset(path)
        # DEV
        # self.elems = self.elems[0:64]
        # keep track of original data when adding NER annotations
        self.offsets = {}
        self.original_passages = {}
        self.samples = self._create_samples(path)
        self.tokenizer = None
        self.batch_size = args.batch_size if 'batch_size' in args else 1
        self.pad_token_id = self.tokenizer.pad_token_id \
            if self.tokenizer is not None else 0

    def _create_samples(self, path):
        """
        Formats raw examples to desired form. Any passages/questions longer
        than max sequence length will be truncated.

        Returns:
            A list of words (string).
        """

        samples = []
        for elem in self.elems:
            # Unpack the context paragraph. Shorten to max sequence length.
            passage = [
                token.lower() for (token, offset) in elem['context_tokens']
            ][:self.args.max_context_length]

            passage_offsets = []
            annotated_passage = []
            passage_tags = []
            original_passage = passage
            passage_tags = []
            question_tags = []
            passage_tag_ids = []
            question_tag_ids = []

            # Each passage has several questions associated with it.
            # Additionally, each question has multiple possible answer spans.
            for qa in elem['qas']:
                qid = qa['qid']
                question = [
                    token.lower() for (token, offset) in qa['question_tokens']
                ][:self.args.max_question_length]

                # import ipdb; ipdb.set_trace()
                # TODO: should be a separate method
                if self.args.add_tags == "ner":
                    cache_id = f"{path}-{qid}"
                    if cache_id in CACHE:
                        data = pickle.loads(CACHE.get(cache_id))
                        passage_offsets = data["passage_offsets"]
                        passage = data["passage"]
                        question = data["question"]
                        # print(f"Loaded data {data}")
                    else:
                        if len(annotated_passage) == 0:
                            doc = SPACY_NLP(" ".join(passage))

                            offset_counter = 0

                            for token in doc:
                                annotated_passage.append(token.text)
                                passage_offsets.append(offset_counter)
                                if token.ent_type_ != '':
                                    offset_counter += 1
                                    annotated_passage.append(token.ent_type_)
                            passage = annotated_passage

                        doc = SPACY_NLP(" ".join(question))

                        annotated_question = []
                        for token in doc:
                            annotated_question.append(token.text)
                            if token.ent_type_ != '':
                                annotated_question.append(token.ent_type_)
                        question = annotated_question

                        data = {}
                        data['passage_offsets'] = passage_offsets
                        data['passage'] = passage
                        data['question'] = question

                        # print(f"Storing data {data}")

                        CACHE.set(cache_id, pickle.dumps(data))
                    passage_tag_ids = []
                    question_tag_ids = []
                    self.offsets[qid] = passage_offsets
                    self.original_passages[qid] = original_passage
                elif self.args.add_tags == "ner_deep":
                    cache_id = f"{path}-{qid}-deep"
                    if cache_id in CACHE:
                        data = pickle.loads(CACHE.get(cache_id))
                        passage_tag_ids = data["passage_tags"]
                        question_tag_ids = data["question_tags"]

                        # reconstruct tag VOCAB hash
                        for i, tag in enumerate(passage_tag_ids):
                            if tag not in NER_VOCAB:
                                NER_VOCAB[tag] = len(NER_VOCAB)

                        for i, tag in enumerate(question_tag_ids):
                            if tag not in NER_VOCAB:
                                NER_VOCAB[tag] = len(NER_VOCAB)
                        # print(f"Loaded data {data}")
                    else:
                        if len(passage_tags) == 0:
                            doc = SPACY_NLP(" ".join(passage))
                            passage_tags = doc.to_array(spacy.attrs.ENT_TYPE)
                            passage_tag_ids = torch.zeros(len(passage_tags)).long()

                            for i, tag in enumerate(passage_tags):
                                if tag not in NER_VOCAB:
                                    NER_VOCAB[tag] = len(NER_VOCAB)
                                passage_tag_ids[i] = NER_VOCAB[passage_tags[i]]

                        doc = SPACY_NLP(" ".join(question))

                        question_tags = doc.to_array(spacy.attrs.ENT_TYPE)
                        question_tag_ids = torch.zeros(len(question_tags)).long()

                        for i, tag in enumerate(question_tags):
                            if tag not in NER_VOCAB:
                                NER_VOCAB[tag] = len(NER_VOCAB)
                            question_tag_ids[i] = NER_VOCAB[question_tags[i]]

                        data = {}
                        data['passage_tags'] = passage_tag_ids
                        data['question_tags'] = question_tag_ids

                        # print(f"Storing data {data}")
                        CACHE.set(cache_id, pickle.dumps(data))
                elif self.args.add_tags == "pos":
                    cache_id = f"{path}-{qid}-pos"
                    if False:
                        data = pickle.loads(CACHE.get(cache_id))
                        passage_tag_ids = data["passage_tags"]
                        question_tag_ids = data["question_tags"]

                        # reconstruct tag VOCAB hash
                        for i, tag in enumerate(passage_tag_ids):
                            if tag not in NER_VOCAB:
                                NER_VOCAB[tag] = len(NER_VOCAB)

                        for i, tag in enumerate(question_tag_ids):
                            if tag not in NER_VOCAB:
                                NER_VOCAB[tag] = len(NER_VOCAB)
                        # print(f"Loaded data {data}")
                    else:
                        if len(passage_tags) == 0:
                            doc = SPACY_NLP(" ".join(passage))
                            passage_tags = doc.to_array(spacy.attrs.POS)
                            passage_tag_ids = torch.zeros(len(passage_tags)).long()

                            for i, tag in enumerate(passage_tags):
                                if tag not in NER_VOCAB:
                                    NER_VOCAB[tag] = len(NER_VOCAB)
                                passage_tag_ids[i] = NER_VOCAB[passage_tags[i]]

                        doc = SPACY_NLP(" ".join(question))

                        question_tags = doc.to_array(spacy.attrs.POS)
                        question_tag_ids = torch.zeros(len(question_tags)).long()

                        for i, tag in enumerate(question_tags):
                            if tag not in NER_VOCAB:
                                NER_VOCAB[tag] = len(NER_VOCAB)
                            question_tag_ids[i] = NER_VOCAB[question_tags[i]]

                        data = {}
                        data['passage_tags'] = passage_tag_ids
                        data['question_tags'] = question_tag_ids

                        # print(f"Storing data {data}")
                        # CACHE.set(cache_id, pickle.dumps(data))

                # Select the first answer span, which is formatted as
                # (start_position, end_position), where the end_position
                # is inclusive.
                answers = qa['detected_answers']
                answer_start, answer_end = answers[0]['token_spans'][0]

                # Adjust the start, end position to include tags
                if self.args.add_tags == "ner":
                    if answer_start > self.args.max_context_length:
                        # since we're doing variable length now,
                        # make sure this is really outside of the tagged sentence
                        answer_start = 4*self.args.max_context_length

                    answer_start += passage_offsets[min(answer_start, len(passage_offsets)-1)]

                    answer_end += passage_offsets[min(answer_end, len(passage_offsets)-1)]
                    if answer_end > self.args.max_context_length:
                        # since we're doing variable length now,
                        # make sure this is really outside of the tagged sentence
                        answer_end = 4*self.args.max_context_length

                # import ipdb; ipdb.set_trace()

                samples.append(
                    (qid, passage, question, answer_start, answer_end, passage_tag_ids, question_tag_ids)
                )
        return samples

    def _create_data_generator(self, shuffle_examples=False):
        """
        Converts preprocessed text data to Torch tensors and returns a
        generator.

        Args:
            shuffle_examples: If `True`, shuffle examples. Default: `False`

        Returns:
            A generator that iterates through all examples one by one.
            (Tuple of tensors)
        """
        if self.tokenizer is None:
            raise RuntimeError('error: no tokenizer registered')

        example_idxs = list(range(len(self.samples)))
        if shuffle_examples:
            shuffle(example_idxs)

        qids = []
        passages = []
        questions = []
        start_positions = []
        end_positions = []
        passages_tags = []
        questions_tags = []
        for idx in example_idxs:
            # Unpack QA sample and tokenize passage/question.
            qid, passage, question, answer_start, answer_end, passage_tags, question_tags = self.samples[idx]

            # Convert words to tensor.
            passage_ids = torch.tensor(
                self.tokenizer.convert_tokens_to_ids(passage)
            )
            question_ids = torch.tensor(
                self.tokenizer.convert_tokens_to_ids(question)
            )
            answer_start_ids = torch.tensor(answer_start)
            answer_end_ids = torch.tensor(answer_end)

            # Store each part in an independent list.
            qids.append(qid)
            passages.append(passage_ids)
            questions.append(question_ids)
            start_positions.append(answer_start_ids)
            end_positions.append(answer_end_ids)
            passages_tags.append(passage_tags)
            questions_tags.append(question_tags)

        return zip(passages, questions, start_positions, end_positions, qids, passages_tags, questions_tags)

    def _create_batches(self, generator, batch_size):
        """
        This is a generator that gives one batch at a time. Tensors are
        converted to "cuda" if necessary.

        Args:
            generator: A data generator created by `_create_data_generator`.
            batch_size: Int. The number of example in a mini batch.

        Yields:
            A dictionary of tensors containing a single batch.
        """
        current_batch = [None] * batch_size
        no_more_data = False
        # Loop through all examples.
        while True:
            bsz = batch_size
            # Get examples from generator
            for i in range(batch_size):
                try:
                    current_batch[i] = list(next(generator))
                except StopIteration:  # Run out examples
                    no_more_data = True
                    bsz = i  # The size of the last batch.
                    break
            # Stop if there's no leftover examples
            if no_more_data and bsz == 0:
                break

            passages = []
            questions = []
            start_positions = torch.zeros(bsz)
            end_positions = torch.zeros(bsz)
            max_passage_length = 0
            max_question_length = 0
            passage_tags = []
            question_tags = []
            # Check max lengths for both passages and questions
            for ii in range(bsz):
                passages.append(current_batch[ii][0])
                questions.append(current_batch[ii][1])
                start_positions[ii] = current_batch[ii][2]
                end_positions[ii] = current_batch[ii][3]
                max_passage_length = max(
                    max_passage_length, len(current_batch[ii][0])
                )
                max_question_length = max(
                    max_question_length, len(current_batch[ii][1])
                )
                passage_tags.append(current_batch[ii][5])
                question_tags.append(current_batch[ii][6])

            # Assume pad token index is 0. Need to change here if pad token
            # index is other than 0.
            padded_passages = torch.zeros(bsz, max_passage_length)
            padded_questions = torch.zeros(bsz, max_question_length)
            padded_passage_tags = torch.zeros(bsz, max_passage_length)
            padded_question_tags = torch.zeros(bsz, max_question_length)
            # Pad passages and questions
            for iii, (passage, question, passage_tags, question_tags) in enumerate(zip(passages, questions, passage_tags, question_tags)):
                padded_passages[iii][:len(passage)] = passage
                padded_questions[iii][:len(question)] = question
                # import ipdb; ipdb.set_trace()
                if self.args.add_tags not in ["ner", "none"]:
                  padded_passage_tags[iii][:len(passage)] = passage_tags[:len(passage)]
                  padded_question_tags[iii][:len(question)] = question_tags[:len(question)]

            # Create an input dictionary
            batch_dict = {
                'qids': [batch[4] for batch in current_batch],
                'passages': cuda(self.args, padded_passages).long(),
                'questions': cuda(self.args, padded_questions).long(),
                'start_positions': cuda(self.args, start_positions).long(),
                'end_positions': cuda(self.args, end_positions).long(),
                'padded_passage_tags': cuda(self.args, padded_passage_tags).long(),
                'padded_question_tags': cuda(self.args, padded_question_tags).long()
            }

            if no_more_data:
                if bsz > 0:
                    # This is the last batch (smaller than `batch_size`)
                    yield batch_dict
                break
            yield batch_dict

    def get_batch(self, shuffle_examples=False):
        """
        Returns a data generator that supports mini-batch.

        Args:
            shuffle_examples: If `True`, shuffle examples. Default: `False`

        Returns:
            A data generator that iterates though all batches.
        """
        return self._create_batches(
            self._create_data_generator(shuffle_examples=shuffle_examples),
            self.batch_size
        )

    def register_tokenizer(self, tokenizer):
        """
        Stores `Tokenizer` object as an instance variable.

        Args:
            tokenizer: If `True`, shuffle examples. Default: `False`
        """
        self.tokenizer = tokenizer
    
    def __len__(self):
        return len(self.samples)
